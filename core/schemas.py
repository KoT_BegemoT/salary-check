from pydantic import BaseModel, ConfigDict


class UserSchemaBase(BaseModel):
    name: str
    email: str
    password: str


class CreateUserSchema(UserSchemaBase):
    pass


class UserSchema(UserSchemaBase):
    model_config = ConfigDict(from_attributes=True)

    id: int


class UserTokenSchemaBase(BaseModel):
    access_token: str
    token_type: str
    user: int


class CreateUserTokenSchema(UserTokenSchemaBase):
    pass


class UserTokenSchema(UserTokenSchemaBase):
    model_config = ConfigDict(from_attributes=True)

    id: int


class UserSalarySchemaBase(BaseModel):
    user: int
    salary: int
    next_promotion_date: str


class CreateUserSalarySchema(UserSalarySchemaBase):
    pass


class UserSalarySchema(UserSalarySchemaBase):
    model_config = ConfigDict(from_attributes=True)

    id: int
