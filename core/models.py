from sqlalchemy import ForeignKey, DateTime
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column


class Base(DeclarativeBase):
    __abstract__ = True

    id: Mapped[int] = mapped_column(primary_key=True)


class User(Base):
    __tablename__ = "user"

    name: Mapped[str]
    email: Mapped[str]
    password: Mapped[bytes]


class UserToken(Base):
    __tablename__ = "user_token"

    access_token: Mapped[str]
    token_type: Mapped[str]
    user: Mapped[int] = mapped_column(ForeignKey("user.id"))


class UserSalary(Base):
    __tablename__ = "user_salary"

    user: Mapped[int] = mapped_column(ForeignKey("user.id"))
    salary: Mapped[int]
    next_promotion_date: Mapped[str]
