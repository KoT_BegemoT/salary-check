from pydantic_settings import BaseSettings
from pathlib import Path

BASE_DIR = Path(__file__).parent.parent


class Settings(BaseSettings):
    db_url: str = f"sqlite+aiosqlite:///{BASE_DIR}/db.sqlite3"
    db_echo: bool = False

    jwt_private: Path = BASE_DIR / "core" / "certs" / "jwt-private.pem"
    jwt_public: Path = BASE_DIR / "core" / "certs" / "jwt-public.pem"
    crypto_algorithm: str = "RS256"
    jwt_auth_token_expiry_minutes: int = 3


settings = Settings()
