from fastapi import Depends, HTTPException
from fastapi.security import OAuth2PasswordBearer
from jwt import InvalidTokenError
from sqlalchemy import select, Result
from sqlalchemy.ext.asyncio import AsyncSession

from core.db_helper import db_helper
from core.models import UserSalary
from core.schemas import CreateUserSalarySchema
from tokens.jwt_token_processing import jwt_decode

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/jwt/get_token/")


async def create_user_salary_view(
    user_salary: CreateUserSalarySchema,
    session: AsyncSession = Depends(db_helper.scoped_session_dependency),
):
    user_salary_object = UserSalary(**user_salary.model_dump())
    session.add(user_salary_object)
    await session.commit()
    return user_salary_object


def get_current_token(token: str = Depends(oauth2_scheme)):
    try:
        pyload = jwt_decode(token)
    except InvalidTokenError as e:
        raise HTTPException(status_code=401, detail=str(e))
    return pyload


async def get_user_salary_view(
    pyload: dict = Depends(get_current_token),
    session: AsyncSession = Depends(db_helper.scoped_session_dependency),
):
    user_id = pyload.get("sub")

    user_found_request = select(UserSalary).where(UserSalary.user == user_id)
    result: Result = await session.execute(user_found_request)
    user_salary = result.scalar()

    if user_salary is None:
        raise HTTPException(status_code=404, detail="Salary not found")

    return user_salary
