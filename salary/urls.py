from fastapi import APIRouter, Depends
from sqlalchemy.ext.asyncio import AsyncSession

from core.db_helper import db_helper
from core.schemas import CreateUserSalarySchema, UserSalarySchema
from salary.views import (
    create_user_salary_view,
    get_current_token,
    get_user_salary_view,
)

router = APIRouter(tags=["salary"])


@router.post("/", response_model=UserSalarySchema, status_code=201)
async def create_user_salary(
    user_salary: CreateUserSalarySchema,
    session: AsyncSession = Depends(db_helper.scoped_session_dependency),
):
    return await create_user_salary_view(user_salary, session)


@router.get("/", response_model=UserSalarySchema)
async def get_user_salary(
    user_id: dict = Depends(get_current_token),
    session: AsyncSession = Depends(db_helper.scoped_session_dependency),
):
    return await get_user_salary_view(user_id, session)
