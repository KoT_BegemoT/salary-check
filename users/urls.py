from fastapi import APIRouter, Depends

from sqlalchemy.ext.asyncio import AsyncSession

from core.schemas import UserSchema, CreateUserSchema
from core.db_helper import db_helper
from users.views import (
    get_users_view,
    create_user_view,
    get_user_by_id_view,
    validate_user_email_and_name,
)

router = APIRouter(tags=["users"])


@router.get("/", response_model=list[UserSchema])
async def get_users(
    session: AsyncSession = Depends(db_helper.scoped_session_dependency),
):
    return await get_users_view(session)


@router.get("/{user_id}/", response_model=UserSchema)
async def get_user_by_id(
    user_id: int, session: AsyncSession = Depends(db_helper.scoped_session_dependency)
):
    return await get_user_by_id_view(user_id, session)


@router.post("/", response_model=UserSchema, status_code=201)
async def create_user(
    user_in: CreateUserSchema = Depends(validate_user_email_and_name),
    session: AsyncSession = Depends(db_helper.scoped_session_dependency),
):
    return await create_user_view(user_in, session)
