import bcrypt
from fastapi import Depends, HTTPException, status
from sqlalchemy import Result, select
from sqlalchemy.ext.asyncio import AsyncSession

from core.db_helper import db_helper
from core.models import User
from core.schemas import CreateUserSchema


def hash_password(password: str) -> bytes:
    salt = bcrypt.gensalt()
    hashed_password: bytes = bcrypt.hashpw(password.encode(), salt)
    return hashed_password


async def get_users_view(
    session: AsyncSession = Depends(db_helper.scoped_session_dependency),
):
    db_request = select(User).order_by(User.id)
    result: Result = await session.execute(db_request)
    users = result.scalars().all()
    return list(users)


async def validate_user_email(
    user_in: CreateUserSchema,
    session: AsyncSession = Depends(db_helper.scoped_session_dependency),
):
    db_request = select(User).where(User.email == user_in.email)
    result: Result = await session.execute(db_request)
    user = result.scalar()
    if user:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="Email already registered",
        )

    return user_in


async def validate_user_email_and_name(
    user_in: CreateUserSchema = Depends(validate_user_email),
    session: AsyncSession = Depends(db_helper.scoped_session_dependency),
):
    db_request = select(User).where(User.name == user_in.name)
    result: Result = await session.execute(db_request)
    user = result.scalar()
    if user:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="Username already registered",
        )

    return user_in


async def create_user_view(
    user_in: CreateUserSchema = Depends(validate_user_email_and_name),
    session: AsyncSession = Depends(db_helper.scoped_session_dependency),
):
    user_in.password = hash_password(user_in.password)
    user = User(**user_in.model_dump())

    session.add(user)
    await session.commit()
    return user


async def get_user_by_id_view(
    user_id: int, session: AsyncSession = Depends(db_helper.scoped_session_dependency)
):
    user = await session.get(User, user_id)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="User not found",
        )

    return user
