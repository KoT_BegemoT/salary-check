import pytest
from pydantic_core import ValidationError

from core.schemas import UserSalarySchemaBase


class TestUserSalarySchema:
    def test_user_salary_schema(self):
        user = UserSalarySchemaBase(
            user=1,
            salary=1,
            next_promotion_date="test",
        )
        assert user.user == 1
        assert user.salary == 1
        assert user.next_promotion_date == "test"

    def test_user_salary_schema_empty_user(self):
        data = {
            "salary": 1,
            "next_promotion_date": "test",
        }

        with pytest.raises(ValidationError) as ex:
            salary = UserSalarySchemaBase(**data)

        assert ex.value.error_count() == 1

        ex_info = ex.value.errors()[0]
        assert ex_info["type"] == "missing"
        assert ex_info["loc"] == ("user",)
        assert ex_info["msg"] == "Field required"
        assert ex_info["input"] == {**data}

    def test_user_salary_schema_empty_salary(self):
        data = {
            "user": 1,
            "next_promotion_date": "test",
        }

        with pytest.raises(ValidationError) as ex:
            salary = UserSalarySchemaBase(**data)

        assert ex.value.error_count() == 1

        ex_info = ex.value.errors()[0]
        assert ex_info["type"] == "missing"
        assert ex_info["loc"] == ("salary",)
        assert ex_info["msg"] == "Field required"
        assert ex_info["input"] == {**data}

    def test_user_salary_schema_empty_promotion_date(self):
        data = {
            "user": 1,
            "salary": 1,
        }

        with pytest.raises(ValidationError) as ex:
            salary = UserSalarySchemaBase(**data)

        assert ex.value.error_count() == 1

        ex_info = ex.value.errors()[0]
        assert ex_info["type"] == "missing"
        assert ex_info["loc"] == ("next_promotion_date",)
        assert ex_info["msg"] == "Field required"
        assert ex_info["input"] == {**data}

    def test_user_salary_schema_invalid_user(self):
        data = {
            "user": "test",
            "salary": 1,
            "next_promotion_date": "test",
        }

        with pytest.raises(ValidationError) as ex:
            salary = UserSalarySchemaBase(**data)

        assert ex.value.error_count() == 1

        ex_info = ex.value.errors()[0]
        assert ex_info["type"] == "int_parsing"
        assert ex_info["loc"] == ("user",)
        assert (
            ex_info["msg"]
            == "Input should be a valid integer, unable to parse string as an integer"
        )
        assert ex_info["input"] == data["user"]

    def test_user_salary_schema_invalid_salary(self):
        data = {
            "user": 1,
            "salary": "test",
            "next_promotion_date": "test",
        }

        with pytest.raises(ValidationError) as ex:
            salary = UserSalarySchemaBase(**data)

        assert ex.value.error_count() == 1

        ex_info = ex.value.errors()[0]
        assert ex_info["type"] == "int_parsing"
        assert ex_info["loc"] == ("salary",)
        assert (
            ex_info["msg"]
            == "Input should be a valid integer, unable to parse string as an integer"
        )
        assert ex_info["input"] == data["salary"]

    def test_user_salary_schema_invalid_promotion_date(self):
        data = {
            "user": 1,
            "salary": 1,
            "next_promotion_date": 1,
        }

        with pytest.raises(ValidationError) as ex:
            salary = UserSalarySchemaBase(**data)

        assert ex.value.error_count() == 1

        ex_info = ex.value.errors()[0]
        assert ex_info["type"] == "string_type"
        assert ex_info["loc"] == ("next_promotion_date",)
        assert ex_info["msg"] == "Input should be a valid string"
        assert ex_info["input"] == data["next_promotion_date"]
