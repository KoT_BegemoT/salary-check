import pytest
from pydantic_core import ValidationError

from core.schemas import UserSchemaBase


class TestUserSchema:
    def test_user_schema(self):
        user = UserSchemaBase(
            name="test",
            email="test",
            password="test",
        )
        assert user.name == "test"
        assert user.email == "test"
        assert user.password == "test"

    def test_user_schema_empty_name(self):
        data = {
            "email": "email",
            "password": "password",
        }

        with pytest.raises(ValidationError) as ex:
            user = UserSchemaBase(**data)

        assert ex.value.error_count() == 1

        ex_info = ex.value.errors()[0]
        assert ex_info["type"] == "missing"
        assert ex_info["loc"] == ("name",)
        assert ex_info["msg"] == "Field required"
        assert ex_info["input"] == {**data}

    def test_user_schema_empty_email(self):
        data = {
            "name": "name",
            "password": "password",
        }

        with pytest.raises(ValidationError) as ex:
            user = UserSchemaBase(**data)

        assert ex.value.error_count() == 1

        ex_info = ex.value.errors()[0]
        assert ex_info["type"] == "missing"
        assert ex_info["loc"] == ("email",)
        assert ex_info["msg"] == "Field required"
        assert ex_info["input"] == {**data}

    def test_user_schema_empty_password(self):
        data = {
            "name": "name",
            "email": "email",
        }

        with pytest.raises(ValidationError) as ex:
            user = UserSchemaBase(**data)

        assert ex.value.error_count() == 1

        ex_info = ex.value.errors()[0]
        assert ex_info["type"] == "missing"
        assert ex_info["loc"] == ("password",)
        assert ex_info["msg"] == "Field required"
        assert ex_info["input"] == {**data}

    def test_user_schema_invalid_name(self):
        data = {
            "name": 1,
            "email": "email",
            "password": "password",
        }

        with pytest.raises(ValidationError) as ex:
            user = UserSchemaBase(**data)

        assert ex.value.error_count() == 1

        ex_info = ex.value.errors()[0]
        assert ex_info["type"] == "string_type"
        assert ex_info["loc"] == ("name",)
        assert ex_info["msg"] == "Input should be a valid string"
        assert ex_info["input"] == data["name"]

    def test_user_schema_invalid_email(self):
        data = {
            "name": "name",
            "email": 1,
            "password": "password",
        }

        with pytest.raises(ValidationError) as ex:
            user = UserSchemaBase(**data)

        assert ex.value.error_count() == 1

        ex_info = ex.value.errors()[0]
        assert ex_info["type"] == "string_type"
        assert ex_info["loc"] == ("email",)
        assert ex_info["msg"] == "Input should be a valid string"
        assert ex_info["input"] == data["email"]

    def test_user_schema_invalid_password(self):
        data = {
            "name": "name",
            "email": "email",
            "password": 1,
        }

        with pytest.raises(ValidationError) as ex:
            user = UserSchemaBase(**data)

        assert ex.value.error_count() == 1

        ex_info = ex.value.errors()[0]
        assert ex_info["type"] == "string_type"
        assert ex_info["loc"] == ("password",)
        assert ex_info["msg"] == "Input should be a valid string"
        assert ex_info["input"] == data["password"]
