import asyncio

import pytest
import pytest_asyncio

from fastapi.testclient import TestClient
from httpx import AsyncClient

from core.db_helper import DatabaseHelper, db_helper
from core.models import Base
from main import app

TEST_DB_URL = "sqlite+aiosqlite:///:memory:"
ECHO = False

test_db_helper = DatabaseHelper(url=TEST_DB_URL, echo=ECHO)


app.dependency_overrides[db_helper.scoped_session_dependency] = (
    test_db_helper.get_scoped_session
)

client = TestClient(app)


@pytest_asyncio.fixture(autouse=True, scope="function")
async def prepare_db():
    async with test_db_helper.engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)
    yield
    async with test_db_helper.engine.begin() as conn:
        await conn.run_sync(Base.metadata.drop_all)


@pytest_asyncio.fixture(scope="session")
async def ac():
    async with AsyncClient(app=app, base_url="http://test") as ac:
        yield ac
