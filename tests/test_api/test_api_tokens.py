from httpx import AsyncClient


class TestTokens:
    async def test_get_token_empty_forms(self, ac: AsyncClient):
        response = await ac.post("/jwt/get_token/")
        assert response.status_code == 422
        response_data = response.json()["detail"]

        assert len(response_data) == 2
        assert response_data[0]["type"] == "missing"
        assert response_data[0]["loc"] == ["body", "username"]
        assert response_data[1]["type"] == "missing"
        assert response_data[1]["loc"] == ["body", "password"]

    async def test_get_token_invalid_user(self, ac: AsyncClient):
        response = await ac.post(
            "/jwt/get_token/",
            data={
                "username": "test",
                "password": "test",
            },
        )
        assert response.status_code == 403
        response_data = response.json()

        assert response_data == {"detail": "Invalid username or password"}

    async def test_get_token(self, ac: AsyncClient):
        user = {
            "name": "test name",
            "email": "test email",
            "password": "test password",
        }

        await ac.post("/users/", json=user)

        response = await ac.post(
            "/jwt/get_token/",
            data={
                "username": user["email"],
                "password": user["password"],
            },
        )
        response_data = response.json()

        assert response.status_code == 201
        assert "access_token" in response_data
        assert response_data["token_type"] == "Bearer"
