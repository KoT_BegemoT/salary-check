from httpx import AsyncClient

from tokens.jwt_token_processing import jwt_encode


class TestSalary:
    async def test_create_salary(self, ac: AsyncClient):
        response = await ac.post(
            "/salary/",
            json={
                "user": 1,
                "salary": 10000,
                "next_promotion_date": "2024-1-06",
            },
        )
        response_data = response.json()

        assert response.status_code == 201
        assert response_data["user"] == 1
        assert response_data["salary"] == 10000
        assert response_data["next_promotion_date"] == "2024-1-06"

    async def test_get_salary(self, ac: AsyncClient):
        response = await ac.get("/salary/")
        response_data = response.json()

        assert response.status_code == 401
        assert response_data == {"detail": "Not authenticated"}

    async def test_get_salary_token_not_enough_segments(self, ac: AsyncClient):
        response = await ac.get("/salary/", headers={"Authorization": "Bearer invalid"})
        response_data = response.json()

        assert response.status_code == 401
        assert response_data == {"detail": "Not enough segments"}

    async def test_get_salary_invalid_token(self, ac: AsyncClient):
        jwt_payload = {"sub": 100, "name": "test name", "email": "test email"}

        token = jwt_encode(jwt_payload)

        response = await ac.get(
            "/salary/", headers={"Authorization": f"Bearer {token}"}
        )
        response_data = response.json()

        assert response.status_code == 404
        assert response_data == {"detail": "Salary not found"}

    async def test_get_salary_valid_token(self, ac: AsyncClient):
        user = {
            "name": "test name",
            "email": "test email",
            "password": "test password",
        }

        user_object = await ac.post("/users/", json=user)  # create user

        salary = {
            "user": user_object.json()["id"],
            "salary": 10000,
            "next_promotion_date": "2024-1-06",
        }

        await ac.post("salary/", json=salary)  # create salary

        response_token = await ac.post(
            "/jwt/get_token/",
            data={
                "username": user["email"],
                "password": user["password"],
            },
        )  # get token
        token = response_token.json()["access_token"]

        response = await ac.get(
            "/salary/", headers={"Authorization": f"Bearer {token}"}
        )  # get salary

        response_data = response.json()

        assert response.status_code == 200
        assert response_data["user"] == 1
        assert response_data["salary"] == 10000
        assert response_data["next_promotion_date"] == "2024-1-06"
