from httpx import AsyncClient


class TestUsers:
    async def test_get_users_empty(self, ac: AsyncClient):
        response = await ac.get("/users/")
        assert response.status_code == 200
        assert response.json() == []

    async def test_create_user(self, ac: AsyncClient):
        response = await ac.post(
            "/users/",
            json={
                "name": "test name",
                "email": "test email",
                "password": "test password",
            },
        )
        assert response.status_code == 201
        assert response.json()["name"] == "test name"
        assert response.json()["email"] == "test email"
        assert response.json()["password"] != "test password"  # password is hashed

    async def test_create_users_duplicate_name(self, ac: AsyncClient):
        await ac.post(
            "/users/",
            json={
                "name": "name",
                "email": "first user email",
                "password": "first user password",
            },
        )
        response = await ac.post(
            "/users/",
            json={
                "name": "name",
                "email": "second user email",
                "password": "second user password",
            },
        )
        assert response.status_code == 409
        assert response.json() == {"detail": "Username already registered"}

    async def test_create_users_duplicate_email(self, ac: AsyncClient):
        await ac.post(
            "/users/",
            json={
                "name": "firs user name",
                "email": "email",
                "password": "first user password",
            },
        )
        response = await ac.post(
            "/users/",
            json={
                "name": "second user name",
                "email": "email",
                "password": "second user password",
            },
        )
        assert response.status_code == 409
        assert response.json() == {"detail": "Email already registered"}

    async def test_get_users(self, ac: AsyncClient):
        user_1 = {
            "name": "test name",
            "email": "test email",
            "password": "test password",
        }

        user_2 = {
            "name": "test name 2",
            "email": "test email 2",
            "password": "test password 2",
        }

        await ac.post("/users/", json=user_1)
        await ac.post("/users/", json=user_2)

        response = await ac.get("/users/")
        response_data = response.json()

        assert response.status_code == 200
        assert len(response_data) == 2
        assert response_data[0]["name"] == user_1["name"]
        assert response_data[1]["name"] == user_2["name"]

    async def test_get_user(self, ac: AsyncClient):
        user = {
            "name": "test name",
            "email": "test email",
            "password": "test password",
        }

        await ac.post("/users/", json=user)

        response = await ac.get("/users/1/")

        assert response.status_code == 200
        assert response.json()["name"] == user["name"]
        assert response.json()["email"] == user["email"]

    async def test_get_user_not_found(self, ac: AsyncClient):
        response = await ac.get("/users/1/")

        assert response.status_code == 404
        assert response.json() == {"detail": "User not found"}
