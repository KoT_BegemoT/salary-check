from httpx import AsyncClient
from core.models import User, UserToken, UserSalary


async def test_users_model(ac: AsyncClient):
    user = User(
        name="test name",
        email="test email",
        password="test password",
    )

    assert user.name == "test name"
    assert user.email == "test email"
    assert user.password == "test password"


async def test_users_token_model(ac: AsyncClient):
    token = UserToken(
        access_token="test access token",
        token_type="test token type",
        user=1,
    )

    assert token.access_token == "test access token"
    assert token.token_type == "test token type"
    assert token.user == 1


async def test_users_salary_model(ac: AsyncClient):
    salary = UserSalary(salary="test salary", user=1)

    assert salary.salary == "test salary"
    assert salary.user == 1
