from datetime import datetime, timedelta

import jwt
from core.settings import settings


def jwt_encode(
    payload: dict,
    private_key: str = settings.jwt_private.read_text(),
    algorithm: str = settings.crypto_algorithm,
    expire_minutes: int = settings.jwt_auth_token_expiry_minutes,
):
    to_encode = payload.copy()
    now = datetime.utcnow()
    expire = now + timedelta(minutes=expire_minutes)

    to_encode.update(expire=expire, iat=now)
    encoded = jwt.encode(payload, private_key, algorithm=algorithm)
    return encoded


def jwt_decode(
    token: str | bytes,
    public_key: str = settings.jwt_public.read_text(),
    algorithm: str = settings.crypto_algorithm,
):
    decoded = jwt.decode(token, public_key, algorithms=algorithm)
    return decoded
