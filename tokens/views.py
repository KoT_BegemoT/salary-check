from typing import Annotated

import bcrypt
from fastapi import Depends, HTTPException, Response, Form, status
from fastapi.security import HTTPBasicCredentials, HTTPBasic
from sqlalchemy import select, Result
from sqlalchemy.ext.asyncio import AsyncSession

from core.db_helper import db_helper
from tokens.jwt_token_processing import jwt_encode
from core.models import User, UserToken
from core.schemas import CreateUserTokenSchema, UserSchema


def verify_password(password: str, hashed_password: bytes) -> bool:
    return bcrypt.checkpw(password.encode(), hashed_password)


async def validate_auth_user(
    username: str = Form(),
    password: str = Form(),
    session: AsyncSession = Depends(db_helper.scoped_session_dependency),
):
    user_found_request = select(User).where(User.email == username)
    result: Result = await session.execute(user_found_request)
    user = result.scalar()

    if user is None:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Invalid username or password",
        )

    if not verify_password(password, user.password):
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Invalid username or password",
        )

    return user


async def create_token_object(
    user: UserSchema = Depends(validate_auth_user),
    session: AsyncSession = Depends(db_helper.scoped_session_dependency),
):
    jwt_payload = {"sub": user.id, "name": user.name, "email": user.email}
    token = jwt_encode(jwt_payload)

    token_schema = CreateUserTokenSchema(
        access_token=token,
        token_type="Bearer",
        user=user.id,
    )

    token_object = UserToken(**token_schema.model_dump())

    session.add(token_object)
    await session.commit()

    return token_object
