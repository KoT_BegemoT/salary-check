from fastapi import APIRouter, Depends, Response

from sqlalchemy.ext.asyncio import AsyncSession

from tokens.views import validate_auth_user, create_token_object
from core.schemas import UserSchema, UserTokenSchema
from core.db_helper import db_helper

router = APIRouter(tags=["jwt"])


@router.post("/get_token/", response_model=UserTokenSchema, status_code=201)
async def get_token(
    response: Response,
    user_in: UserSchema = Depends(validate_auth_user),
    session: AsyncSession = Depends(db_helper.scoped_session_dependency),
):
    token_object = await create_token_object(user_in, session)

    response.set_cookie(
        key="Authorization",
        value=f"{token_object.token_type} {token_object.access_token}",
    )
    return token_object
