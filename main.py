from contextlib import asynccontextmanager

import uvicorn
from fastapi import FastAPI

from tokens.urls import router as jwt_router
from users.urls import router as users_router
from salary.urls import router as salary_router
from core.models import Base
from core.db_helper import db_helper


@asynccontextmanager
async def lifespan(app: FastAPI):
    async with db_helper.engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)
    yield


app = FastAPI(lifespan=lifespan)
app.include_router(users_router, prefix="/users")
app.include_router(jwt_router, prefix="/jwt")
app.include_router(salary_router, prefix="/salary")


@app.get("/")
async def root():
    return {"message": "Hello World"}


if __name__ == "__main__":
    uvicorn.run("main:app")
