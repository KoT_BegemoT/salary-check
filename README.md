# Тестовое задание на курс «Python»

Задание выполнил Лебедев Павел

Это приложение предоставляет пользователям возможность просмотра зарплаты и даты следующего повышения, при предъявлении
валидного токена.

## Запуск приложения

    docker-compose build
    docker-compose up  

## Запуск тестов

    poetry install
    pytest ./tests

# REST API

`GET /users/`

- Получение всех пользователей в виде списка.

`POST /users/`

- Создание нового пользователя

Пример запроса:

    curl -X 'POST' \
      'http://127.0.0.1:8000/users/' \
      -H 'accept: application/json' \
      -H 'Content-Type: application/json' \
      -d '{
      "name": "string",
      "email": "string",
      "password": "string"}'

`GET /users/{user_id}`

- Получение данных о пользователей по id

`POST /jwt/get_token/`

- Получение jwt токена для пользователя. Требует логин и пароль передаваемые в виде формы.

Пример запроса:

    curl -X 'POST' \
      'http://127.0.0.1:8000/jwt/get_token/' \
      -H 'accept: application/json' \
      -H 'Content-Type: application/x-www-form-urlencoded' \
      -d 'username=user&password=user'

`POST /salary/`

- Создание нового элемента таблицы UserSalary, описывающего зарплату и дату следующего повышения пользователя.

Пример запроса:

    curl -X 'POST' \
      'http://127.0.0.1:8000/salary/' \
      -H 'accept: application/json' \
      -H 'Content-Type: application/json' \
      -d '{
      "user": 0,
      "salary": 0,
      "next_promotion_date": "string"}'

`GET /salary/`

- Получение информации о зарплате пользователя. Требует jwt токен передаваемый в заголовке.

Пример запроса:

    curl -X 'GET' \
      'http://127.0.0.1:8000/salary/' \
      -H 'accept: application/json' \
      -H 'Authorization: Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjIsIm5hbWUiOiJ1c2VyIiwiZW1haWwiOiJ1c2VyIn0.GL0ZkI9dBkqSqd3vtXKxZgsHEsTvWgZG74Ql-5jsqsUNHmxLnGxrZygHoAKdAdMVG_6_mz9rY5ehp0am8SOMTwYuJdW7qP3cVuuLhhkEO_sgH0_NbrJKHDQy8x2ztf9EwY2M-c5xVesQC6tUhaUcY0OYf37wqe8ptfaWTG6mjgwBLQJnrc6paGNv0pGJVZ0U1ykHS7zZUUdDF78e7lGRyt7L6QDZupKDmsS32N88xMxol2J5xHol1a-d9XqWR-PYRzJ6Sgp4va-NsB1F4ef6MlLUWuiyGvrkJdoOf71Oc5FSlWNxpRr6GUaHvWwRzoWutjr43qJpSNPTMn7HvZ1Aqg'
